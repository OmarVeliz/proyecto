package com.cine.cartelera;

public class Cartelera {

	private Integer idCartelera;
	private Integer idSala;
	private Integer idPelicula;
	private String horaInicio;
	private String horaFin;
	
	public Integer getIdCartelera() {
		return idCartelera;
	}



	public void setIdCartelera(Integer idCartelera) {
		this.idCartelera = idCartelera;
	}



	public Integer getIdSala() {
		return idSala;
	}



	public void setIdSala(Integer idSala) {
		this.idSala = idSala;
	}



	public Integer getIdPelicula() {
		return idPelicula;
	}



	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}



	public String getHoraInicio() {
		return horaInicio;
	}



	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}



	public String getHoraFin() {
		return horaFin;
	}



	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}



	public Cartelera(Integer idCartelera, Integer idSala, Integer idPelicula,
			String horaInicio, String horaFin) {
		super();
		this.idCartelera = idCartelera;
		this.idSala = idSala;
		this.idPelicula = idPelicula;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
	}



	public Cartelera() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
