package com.cine.sala;

public class Sala {

	private Integer idSala;
	private Integer idComplejo;
	private String nombreSala;
	private String tipoSala;
	
	public Integer getIdSala() {
		return idSala;
	}
	public void setIdSala(Integer idSala) {
		this.idSala = idSala;
	}
	public Integer getIdComplejo() {
		return idComplejo;
	}
	public void setIdComplejo(Integer idComplejo) {
		this.idComplejo = idComplejo;
	}
	public String getNombreSala() {
		return nombreSala;
	}
	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}
	public String getTipoSala() {
		return tipoSala;
	}
	public void setTipoSala(String tipoSala) {
		this.tipoSala = tipoSala;
	}
	
	public Sala(Integer idSala, Integer idComplejo, String nombreSala,
			String tipoSala) {
		super();
		this.idSala = idSala;
		this.idComplejo = idComplejo;
		this.nombreSala = nombreSala;
		this.tipoSala = tipoSala;
	}
	
	public Sala() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
