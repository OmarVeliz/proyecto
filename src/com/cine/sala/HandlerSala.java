package com.cine.sala;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.complejo.Complejos;
import com.cine.complejo.handler.ManejadorComplejo;
import com.cine.database.DataBase;
import com.cine.sala.Sala;

public class HandlerSala {

private static final HandlerSala INSTANCE = new HandlerSala();
	
	public static final HandlerSala getInstance(){
		return INSTANCE;
	}
	
	public Sala getSalaById(Integer id){
		
		Sala sal = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Sala WHERE idComplejo = " + id);
		
		try {
			while(result.next()){
				sal = new Sala(result.getInt("idSala"), result.getInt("idComplejo"), result.getString("nombreSala"), result.getString("tipoSala"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sal;
	}
	
	public ArrayList<Sala> getSalaList(Integer id){
		
		ArrayList<Sala> list = new ArrayList<Sala>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Sala where idComplejo = " + id);
		
		try {
			while(result.next()){
				list.add(new Sala(result.getInt("idSala"), result.getInt("idComplejo"), result.getString("nombreSala"), result.getString("tipoSala")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
}
