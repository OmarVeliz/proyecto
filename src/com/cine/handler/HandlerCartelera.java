package com.cine.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.cine.cartelera.Cartelera;
import com.cine.database.DataBase;

public class HandlerCartelera {

	private static final HandlerCartelera INSTANCE = new HandlerCartelera();
	
	public static final HandlerCartelera getInstance(){
		return INSTANCE;
	}
	
	public Cartelera getCarteleraById(Integer idC){
		
		Cartelera pelC = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Cartelera WHERE idPelicula = " + idC);
		
		try {
			while(result.next()){
				pelC = new Cartelera(result.getInt("idCartelera"), result.getInt("idSala"), result.getInt("idPelicula"), result.getString("horaInicio"), result.getString("horaFin"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pelC;
	}
}
