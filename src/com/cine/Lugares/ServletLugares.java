package com.cine.Lugares;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletLugares.do")
public class ServletLugares extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private String nombre;
	private String funcion;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		nombre = request.getParameter("nombre");
		funcion = request.getParameter("Funcion");
		
		request.setAttribute("nombre", nombre);
		request.setAttribute("Funcion", funcion);
		
		request.getRequestDispatcher("CineKinal/Descripciones/src/Lugares.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
