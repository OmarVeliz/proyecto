package com.cine.complejo.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.database.DataBase;
import com.cine.complejo.Complejos;

public class ManejadorComplejo {
	
	private static final ManejadorComplejo INSTANCE = new ManejadorComplejo();
	
	public static final ManejadorComplejo getInstance(){
		return INSTANCE;
	}
	
	public Complejos getComplejoById(Integer id){
		
		Complejos emp = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Complejo WHERE idComplejo = " + id);
		
		try {
			while(result.next()){
				emp = new Complejos(result.getInt("idComplejo"), result.getString("nombreComplejo"), result.getString("direccionComplejo"), result.getInt("callCenter"), result.getString("latitudComplejo"), result.getString("longitudComplejo"), result.getString("rutaServerlet"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return emp;
	}
	
	public ArrayList<Complejos> getComplejoList(){
		
		ArrayList<Complejos> list = new ArrayList<Complejos>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Complejo");
		
		try {
			while(result.next()){
				list.add(new Complejos(result.getInt("idComplejo"), result.getString("nombreComplejo"), result.getString("direccionComplejo"), result.getInt("callCenter"), result.getString("latitudComplejo"), result.getString("longitudComplejo"), result.getString("rutaServerlet")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
	/*public ArrayList<Complejos> getEmployeeListByDepartmentId(Integer id){
		
		ArrayList<Complejos> list = new ArrayList<Complejos>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT e.ID, e.NAME, e.LNAME, e.PHONE FROM (employees e INNER JOIN employee_department ed ON e.ID = ed.IDEMP) INNER JOIN departments d ON ed.IDDEP = d.ID WHERE d.ID = " + id);
		
		try {
			while(result.next()){
				list.add(new Employee(result.getInt("ID"), result.getString("NAME"), result.getString("LNAME"), result.getString("PHONE")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}*/
}
