package com.cine.complejo;

public class Complejos {

	private Integer idComplejo;
	private String  nombreComplejo;
	private String  direccionComplejo;
	private Integer callCenter;
	private String  latitudComplejo;
	private String  longitudComplejo;
	private String	rutaServerlet;
	public Integer getIdComplejo() {
		return idComplejo;
	}
	public void setIdComplejo(Integer idComplejo) {
		this.idComplejo = idComplejo;
	}
	public String getNombreComplejo() {
		return nombreComplejo;
	}
	public void setNombreComplejo(String nombreComplejo) {
		this.nombreComplejo = nombreComplejo;
	}
	public String getDireccionComplejo() {
		return direccionComplejo;
	}
	public void setDireccionComplejo(String direccionComplejo) {
		this.direccionComplejo = direccionComplejo;
	}
	public Integer getCallCenter() {
		return callCenter;
	}
	public void setCallCenter(Integer callCenter) {
		this.callCenter = callCenter;
	}
	public String getLatitudComplejo() {
		return latitudComplejo;
	}
	public void setLatitudComplejo(String latitudComplejo) {
		this.latitudComplejo = latitudComplejo;
	}
	public String getLongitudComplejo() {
		return longitudComplejo;
	}
	public void setLongitudComplejo(String longitudComplejo) {
		this.longitudComplejo = longitudComplejo;
	}
	public String getRutaServerlet() {
		return rutaServerlet;
	}
	public void setRutaServerlet(String rutaServerlet) {
		this.rutaServerlet = rutaServerlet;
	}
	public Complejos() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Complejos(Integer idComplejo, String nombreComplejo,
			String direccionComplejo, Integer callCenter,
			String latitudComplejo, String longitudComplejo,
			String rutaServerlet) {
		super();
		this.idComplejo = idComplejo;
		this.nombreComplejo = nombreComplejo;
		this.direccionComplejo = direccionComplejo;
		this.callCenter = callCenter;
		this.latitudComplejo = latitudComplejo;
		this.longitudComplejo = longitudComplejo;
		this.rutaServerlet = rutaServerlet;
	}
	
	
	
}
