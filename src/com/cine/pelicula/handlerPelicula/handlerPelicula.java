package com.cine.pelicula.handlerPelicula;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cine.complejo.Complejos;
import com.cine.complejo.handler.ManejadorComplejo;
import com.cine.database.DataBase;
import com.cine.pelicula.Pelicula;
import com.cine.cartelera.Cartelera;

public class handlerPelicula {

private static final handlerPelicula INSTANCE = new handlerPelicula();
	
	public static final handlerPelicula getInstance(){
		return INSTANCE;
	}
	
	public Pelicula getPeliculaById(Integer id){
		
		Pelicula pel = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula WHERE idPelicula = " + id);
		
		try {
			while(result.next()){
				pel = new Pelicula(result.getInt("idPelicula"), result.getString("nombrePelicula"), result.getString("descripcionPelicula"), result.getString("trailerPelicula"), result.getInt("duracionPelicula"), result.getString("rutaImg"), result.getString("direccionServlet"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pel;
	}
	
	public ArrayList<Pelicula> getPeliculaList(){
		
		ArrayList<Pelicula> list = new ArrayList<Pelicula>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula");
		
		try {
			while(result.next()){
				list.add(new Pelicula(result.getInt("idPelicula"), result.getString("nombrePelicula"), result.getString("descripcionPelicula"), result.getString("trailerPelicula"), result.getInt("duracionPelicula"), result.getString("rutaImg"), result.getString("direccionServlet")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
}
