package com.cine.pelicula.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cine.handler.HandlerCartelera;
import com.cine.pelicula.handlerPelicula.handlerPelicula;

/**
 * Servlet implementation class ServletIron
 */
@WebServlet("/ServletIron.do")
public class ServletIron extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletIron() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			if (request.getParameter("idPelicula") != null){
			
			Integer idPel = Integer.parseInt(request.getParameter("idPelicula"));
			
			request.setAttribute("cartelera", HandlerCartelera.getInstance().getCarteleraById(idPel));
			
			request.setAttribute("pelicula", handlerPelicula.getInstance().getPeliculaById(idPel));
			request.setAttribute("idComplejo", idPel);
			

			request.getRequestDispatcher("CineKinal/Descripciones/src/IronMan.jsp").forward(request, response);
				
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
