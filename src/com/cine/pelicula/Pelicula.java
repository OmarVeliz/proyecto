package com.cine.pelicula;

public class Pelicula {
	
	private Integer idPelicula;
	private String nombrePelicula;
	private String descripcionPelicula;
	private String trailerPelicula;
	private Integer duracionPelicula;
	private String rutaImg;
	private String direccionServlet;
	
	public Integer getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getNombrePelicula() {
		return nombrePelicula;
	}
	public void setNombrePelicula(String nombrePelicula) {
		this.nombrePelicula = nombrePelicula;
	}
	public String getDescripcionPelicula() {
		return descripcionPelicula;
	}
	public void setDescripcionPelicula(String descripcionPelicula) {
		this.descripcionPelicula = descripcionPelicula;
	}
	public String getTrailerPelicula() {
		return trailerPelicula;
	}
	public void setTrailerPelicula(String trailerPelicula) {
		this.trailerPelicula = trailerPelicula;
	}
	public Integer getDuracionPelicula() {
		return duracionPelicula;
	}
	public void setDuracionPelicula(Integer duracionPelicula) {
		this.duracionPelicula = duracionPelicula;
	}
	public String getRutaImg() {
		return rutaImg;
	}
	public void setRutaImg(String rutaImg) {
		this.rutaImg = rutaImg;
	}
	public String getDireccionServlet() {
		return direccionServlet;
	}
	public void setDireccionServlet(String direccionServlet) {
		this.direccionServlet = direccionServlet;
	}
	
	public Pelicula(Integer idPelicula, String nombrePelicula,
			String descripcionPelicula, String trailerPelicula,
			Integer duracionPelicula, String rutaImg, String direccionServlet) {
		super();
		this.idPelicula = idPelicula;
		this.nombrePelicula = nombrePelicula;
		this.descripcionPelicula = descripcionPelicula;
		this.trailerPelicula = trailerPelicula;
		this.duracionPelicula = duracionPelicula;
		this.rutaImg = rutaImg;
		this.direccionServlet = direccionServlet;
	}
	
	public Pelicula() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}
