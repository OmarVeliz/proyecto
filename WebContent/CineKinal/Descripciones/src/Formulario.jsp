<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link type="text/css" rel="stylesheet" href="stylesheet.css"/>
  <link type="text/css" rel="stylesheet" href="/CineKinal2013173/CineKinal/Descripciones/src/lugares.css"/> 
<title>Formulario</title>
</head>
<body class="bodyLugares">

      <form action="ServletCorreo" method="post" onsubmit="return validacion()">
      <h1 color="red"> <i> Pelicula: ${nombre} </i></h1>
      
        <legend class="animated rubberBand"><h1>Informacion personal:</h1></legend>
          <br></br>
          Primer nombre:<br>
          <input class="animated wobble" type="text" name="NombreCliente" id="nombreC"/>
          <br>
          Telefono:<br>
          <input class="animated wobble" type="text" name="NumeroTelefonico" id="Telefono" pattern="[0-8]{8}">
          <br/>
          Targeta de credito:<br>
          <input class="animated wobble" type="number" name="TarjetaCredito" id="TarjetaCredito">
          <br>
            Seleccione su tarjeta:<br>
          <select class="animated wobble" name="cardtypelist" size="1">
            <option selected value="1">Mastercard</option>
            <option value="2">Visa</option>
            <option value="3">American Express</option>
            <option value="4">Discover</option>
          </select>
          <br/>
          Correo electronico:<br>
          <input type="text" name="CorreoElect" id="Correo" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$">
          <br></br>
          <br></br>
          <input class="animated wobble" type="submit" value="Aceptar">
        </form> 
        
        <br/>
        <address>
        Written by Omar Jacobo Muñoz Veliz.<br> 
        Correo:<br>
        omar1123veliz@gmail.com<br>
        Guatemala, guatemala<br>
        Guatemala
        </address>
	  
	    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	  <script type="text/javascript" src="/CineKinal2013173/CineKinal/Descripciones/src/js/jquery-1.11.2.min.js"></script>
	  <script type="text/javascript" src="/CineKinal2013173/CineKinal/Descripciones/src/js/script.js"></script>
	  <script type="text/javascript" src="/CineKinal2013173/CineKinal/Descripciones/src/js/Cambio.js"></script>
	  <script type="text/javascript" src="/CineKinal2013173/CineKinal/Descripciones/src/js/lugares.js"></script>
</body>
</html>