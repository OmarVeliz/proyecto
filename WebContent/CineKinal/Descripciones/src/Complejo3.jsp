<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta charset="utf-8">
  <title>Complejos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="CineKinal/Descripciones/src/css/bootstrap.min.css" rel="stylesheet">
	<link href="CineKinal/Descripciones/src/css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/jquery.min.js"></script>
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/scripts.js"></script>
		
		<script type="text/javascript"><!--
			    function initialize() {
			        var latlng = new google.maps.LatLng(14.607144, -90.485727);
			        var settings = {
			            zoom: 15,
			            center: latlng,
			            mapTypeControl: true,
			            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			            navigationControl: true,
			            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
			            mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
			        
			    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);
			    
			    var companyPos = new google.maps.LatLng(57.0442, 9.9116);
			    var companyMarker = new google.maps.Marker({
			        position: companyPos,
			        map: map,
			        title:"Pradera"
			    });
			    
			    var companyLogo = new google.maps.MarkerImage('images/logo.png',
			    	    new google.maps.Size(100,50),
			    	    new google.maps.Point(0,0),
			    	    new google.maps.Point(50,50)
			    	);
			    	var companyShadow = new google.maps.MarkerImage('images/logo_shadow.png',
			    	    new google.maps.Size(130,50),
			    	    new google.maps.Point(0,0),
			    	    new google.maps.Point(65, 50)
			    	);
			    	var companyPos = new google.maps.LatLng(57.0442, 9.9116);
			    	var companyMarker = new google.maps.Marker({
			    	    position: companyPos,
			    	    map: map,
			    	    icon: companyLogo,
			    	    shadow: companyShadow,
			    	    title:"Company Title"
			    	});
				
			}
			</script>
			
</head>
<body onload="initialize()">
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 align="Center">
				Cayala
			</h3>
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>
							#Sala
						</th>
						<th>
							Complejo
						</th>
						<th>
							Descripcion
						</th>
						<th>
							Tipo
						</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${sala3}" var="sal">
					<tr>
						<td>
						${sal.getIdSala()}
						</td>
						<td>
						${sal.getIdComplejo()}
						</td>
						<td>
						${sal.getNombreSala()}
						</td>
						<td>
						${sal.getTipoSala()}
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			
			<br/>
			<br/>
			<br/>
		
			<div id="map_canvas" style="width:800px; height:500px"></div>
			
			<br/>
			<br/>
			<br/>


		</div>
	</div>
</div>
</body>
</html>