<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta charset="utf-8">
  <title>Dorian Gray</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="auDorianGray" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="CineKinal/Descripciones/src/css/bootstrap.min.css" rel="stylesheet">
	<link href="CineKinal/Descripciones/src/css/style.css" rel="stylesheet">
	
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/jquery.min.js"></script>
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/scripts.js"></script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="carousel slide" id="carousel-299319">
				<ol class="carousel-indicators">
					<li data-slide-to="0" data-target="#carousel-299319">
					</li>
					<li data-slide-to="1" data-target="#carousel-299319">
					</li>
					<li data-slide-to="2" data-target="#carousel-299319" class="active">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item">
						<img alt="" src="CineKinal/Descripciones/src/img/carouselDor1.jpg" width="1200px" height="300px">
						<div class="carousel-caption">
							<h4>
							
							</h4>
							<p>
					
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="CineKinal/Descripciones/src/img/carouselDor2.jpg" width="1200px" height="300px">
						<div class="carousel-caption">
							<h4>
								
							</h4>
							<p>
							
							</p>
						</div>
					</div>
					<div class="item active">
						<img alt="" src="CineKinal/Descripciones/src/img/carouselDor3.jpg" width="1200px" height="300px">
						<div class="carousel-caption">
							<h4>
								
							</h4>
							<p>
							
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-299319" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-299319" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
			<br></br>
			<br></br>
			
			<div class="row clearfix">
				<div class="col-md-6 column">
					<img src="CineKinal/Descripcion(Dorian).jpg" height="500px" width="500px">
			</div>
				<div class="col-md-6 column">
					<dl>
						<dt>
							<h1>${pelicula.getNombrePelicula()}</h1>
 						</dt>
						<dd>
						${pelicula.getTrailerPelicula()}
						</dd>
						<br></br>
						<dt>
							Duracion: ${pelicula.getDuracionPelicula()} horas.
						</dt>
							<br/>
						<dd>
						Sala: ${cartelera.getIdSala()}
						</dd>
					</dl>
				</div>
			</div>
			<br></br>
			<br></br>
			<table class="table">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							Lunes
						</th>
						<th>
							Martes
						</th>
						<th>
							Miercoles
						</th>
						<th>
							Jueves
						</th>
						<th>
							Viernes
						</th>
						<th>
							Sabado
						</th><th>
							Domingo
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							1
						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
 						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
						</td>
						<td>
							<a href="/CineKinal2013173/ServletLugares.do?nombre=DorianGray&Funcion=${pelicula.getDuracionPelicula()}"> ${cartelera.getHoraInicio()} - ${cartelera.getHoraFin()} </a>
						</td>
						
					</tr>
					
				</tbody>
			</table>
			
			<br/>
			<br/>
			  <iframe width="1000" height="400" src="https://www.youtube.com/embed/5eAQWllCHHU" frameborder="0" allowfullscreen>
		</div>
	</div>
</div>
</body>
</html>