<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta charset="utf-8">
  <title>Complejos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="CineKinal/Descripciones/src/css/bootstrap.min.css" rel="stylesheet">
	<link href="CineKinal/Descripciones/src/css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/jquery.min.js"></script>
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="CineKinal/Descripciones/src/js/scripts.js"></script>
</head>
<body>
<div class="container">
	<div class="row clearfix">
			<h1> Complejos </h1>	
			<br></br>
			<table class="table">
				<thead>
					<tr>
						<th>
							#IdComplejo
						</th>
						<th>
							NombreComplejo
						</th>
						<th>
							DireccionComplejo
						</th>
						<th>
							CallCenter
						</th>
						<th>
							LatitudComplejo
						</th>
						<th>
							LongitudComplejo
						</th>
					</tr>
				</thead>
					<tbody>
					<c:forEach items="${complejo}" var="comp">
					<tr>
						<td>
							${comp.getIdComplejo()}
						</td>
						<td>
							<a href="${comp.getRutaServerlet()}">${comp.getNombreComplejo()}</a>
						</td>
						<td>
							${comp.getDireccionComplejo()}
						</td>
						<td>
							${comp.getCallCenter()}
						</td>
						<td>
							${comp.getLatitudComplejo()}
						</td>
						<td>
							${comp.getLongitudComplejo()}
						</td>
					</tr>
						 </c:forEach>
				</tbody>
			</table>
	</div>
</div>
</body>
</html>