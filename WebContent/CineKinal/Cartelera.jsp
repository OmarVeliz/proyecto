<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href="stylesheet.css"/>  
<link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
<script src="js/ie-emulation-modes-warning.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link href="carousel.css" rel="stylesheet">
<link href="/CineKinal2013173/CineKinal/pixar.css" rel="stylesheet">
<script src="js/holder.js"></script>
<script type="text/javascript" src="Descripciones/src/js/jquery.min.js"></script>
<script type="text/javascript" src="Descripciones/src/js/bootstrap.min.js"></script>
<script type="text/javascript" src="Descripciones/src/js/scripts.js"></script>
<title>Cartelera</title>
</head>
<body>
	
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h2 class="featurette-heading" align="center"> Cartelera </h2>
		</div>
	</div>
	</div>
	
	
	<div class="pixar">
	  <div class="head">
	    <div class="lamp"></div>
	  </div><br />
	  <div class="body">
	    <div class="arm arm-top"></div>
	    <div class="arm arm-middle"></div>
	    <div class="arm arm-bottom"></div>
	  </div>
	  <div class="footer"></div>
	</div>

	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>

	
    <div class="container marketing">
    
	<c:forEach items="${listado}" var="list">
     <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h1> ${list.getIdPelicula()} </h1>
          <h2 class="featurette-heading"><a href="${list.getDireccionServlet()}"> ${list.getNombrePelicula()} </a><br/><span class="text-muted"><br/></span></h2>
          <br></br>
          <p class="lead">${list.getDescripcionPelicula()}</p>
          <p class="lead">${list.getTrailerPelicula()}</p>
          <p class="lead"><strong> Duracion. </strong></p>
     	  <p class="lead"> ${list.getDuracionPelicula()} </p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="${list.getRutaImg()}" height="500px" width="500px" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      </c:forEach>
      
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Hacks, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

	</div>	
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
 	  <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
 	  <script type="text/javascript" src="js/script.js"></script>

</body>
</html>