--Complejos

INSERT INTO Complejo(IDCOMPLEJO,NOMBRECOMPLEJO,direccionComplejo, callCenter, latitudComplejo, longitudComplejo, rutaServerlet)
  VALUES (Complejo_seq.NEXTVAL, 'Okland', 'Diagonal 6, Guatemala', 8932812, '14.598668', '-90.507011', '/CineKinal2013173/ServletSala.do?idComplejo=2');
  
INSERT INTO Complejo(IDCOMPLEJO,NOMBRECOMPLEJO,direccionComplejo, callCenter, latitudComplejo, longitudComplejo, rutaServerlet)
  VALUES (Complejo_seq.NEXTVAL, 'Miraflores', '21A Avenida, Guatemala', 2389432, '14.621414', '-90.552285', '/CineKinal2013173/ServletSala2.do?idComplejo=4');
    
INSERT INTO Complejo(IDCOMPLEJO,NOMBRECOMPLEJO,direccionComplejo, callCenter, latitudComplejo, longitudComplejo, rutaServerlet)
  VALUES (Complejo_seq.NEXTVAL, 'Cayala', 'Zona 16, Guatemala', 2463-2020, '14.607082', '90.485531','/CineKinal2013173/sevletSala3.do?idComplejo=6');
    
INSERT INTO Complejo(IDCOMPLEJO,NOMBRECOMPLEJO,direccionComplejo, callCenter, latitudComplejo, longitudComplejo, rutaServerlet)
  VALUES (Complejo_seq.NEXTVAL, 'Pradera', 'Carrt.Interamericana 18', 2265-2020, '14.579591', '-90.494513','/CineKinal2013173/ServeltSala4.do?idComplejo=8');
    
--Peliculas
	
INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'El gran Truco', 'La pelicula presenta a dos personajes antagonistas: Bane (Tom Hardy), un mercenario que anteriormente era un miembro de la Liga de las Sombras, quien busca destruir Gotham City y Selina Kyle (Anne Hathaway), una ladrona de guante blanco cuya aparicon en Gotham City pone en marcha una cadena de acontecimientos que conducen a Batman a salir de su retiro.', 'Todo comienza en el agitado Londres de finales del siglo XIX. En una epoca en la que los magos son los idolos mas reconocidos, dos jovenes magos se proponen labrar su propio camino a la fama. El ostentoso y sofisticado Robert Angier es un consumado           
     						artista, mientras el rudo purista Alfred Borden es un genio creativo que carece de la desenvoltura necesaria para mostrar al publico sus magicas ideas.
     						Al principio, son dos compa&ntilde;eros y amigos que se admiran mutuamente. Sin embargo, cuando el mejor truco de ambos se echa a perder, se convierten en enemigos irreconciliables e intentan por todos los medios superar al otro y acabar con el.
     						Truco a truco, espectaculo tras espectaculo, se va fraguando su feroz competicion, que ya no conoce l�mites: llegan incluso a utilizar los nuevos y fant�sticos poderes de la electricidad y la brillantez cientifica de Nikola Tesla.', 02.21, '/CineKinal2013173/CineKinal/1.jpg', '/CineKinal2013173/getPeliculaById.do?idPelicula=2');

INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL,'El caballero de la noche asciende','Bane (Tom Hardy), un terrorista internacional es buscado por el gobierno de los Estados Unidos por su implicacion con un cientifico especializado en energia nuclear.', 'La pelicula presenta a dos personajes antagonistas: Bane (Tom Hardy), un mercenario que anteriormente era un miembro de la Liga de las Sombras.', 03.20, '/CineKinal2013173/CineKinal/2.jpg', '/CineKinal2013173/getBatmanById.do?idPelicula=4');

 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Avengers', 'Cuando el asgardiano Loki, hermano adoptivo de Thor, cae al espacio profundo despues de enfrentarse con el mismo (visto en la pelicula Thor, del universo cinematografico de Marvel), se encuentra con el lider de una raza extraterrestre llamada Chitauri. Este comenta de que a cambio de que Loki le traiga el Teseracto, que es una fuente de energia poderosa de potencial aun desconocida.', ' Cuando el asgardiano Loki, hermano adoptivo de Thor, cae al espacio profundo despues de enfrentarse con el mismo (visto en la pelicula Thor, del universo cinematografico de Marvel), se encuentra con el lider de una raza
					      extraterrestre llamada Chitauri. Este comenta de que a cambio de que Loki le traiga el Teseracto, que es una fuente de energia poderosa que puede abrir un portal entre mundos y tambien de potencial aun desconocido, va a concederle a el un ejercito 
					      para que el pueda ser el due&ntilde;o de la Tierra. Mientras tanto, Nick Fury, director de la agencia S.H.I.E.L.D, y su teniente Maria Hill, llegan a unas dependencias de investigacion durante una evacuacion, debido a que el fisico Erik Selvig estaba experimentando con el Teseracto 
					      para ver su potencial con un equipo de investigacion. El agente Phil Coulson, explica que la razon de la evacuacion se debe a que el objeto empieza a emitir radiacion inusual. Pronto el Teseracto se activa y abre un agujero de gusano, en el que llega Loki de sorpresa a la Tierra. Asi, el toma el elemento y usa su baston para mantener en una especie de "encantamiento"', 02.30, '/CineKinal2013173/CineKinal/3.jpg','/CineKinal2013173/ServletAvengers.do?idPelicula=6');

  INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Dorian Gray', 'Basil Hallward es un artista que queda enormemente impresionado por la belleza estetica de un joven llamado Dorian Gray y comienza a encapricharse con el, creyendo que esta belleza es la responsable de la nueva forma de su arte. Basil pinta un retrato del joven. Charlando en el jardin de Basil, Dorian conoce a Lord Henry Wotton, un amigo de Basil, y empieza a cautivarse por la vision del mundo de Lord Henry.', 'Cuando un joven Dorian Gray (Ben Barnes), de extraordinaria belleza y gran ingenuidad, llega al Londres victoriano, se ve arrastrado a un torbellino social por el carism�tico Henry Wotton (Colin Firth), que inicia a Dorian en los placeres hedonistas que ofrece la ciudad. Basil Hallward (Ben Chaplin), artista de sociedad y amigo de Henry, pinta un retrato de Dorian con el que pretende captar toda la fuerza de su juvenil belleza. Cuando se presenta el cuadro, Dorian realiza una fr�vola afirmaci�n: dar�a cualquier cosa por permanecer tal como aparece en el retrato, hasta su propia alma.
						Alentado por Henry, Dorian sigue adelante con sus alocadas aventuras pero, mientras �l parece seguir tan inocente y hermoso como siempre, su retrato, ahora guardado en el �tico, se va volviendo m�s feo y horroroso con cada maldad que comete. Parece que ahora puede satisfacer cualquier deseo prohibido sin consecuencia alguna para s� mismo; pero, cuando Basil insiste en ver el retrato, Dorian se ve obligado a asesinarlo y huir del pa�s. <br/>
						 ', 2.15, '/CineKinal2013173/CineKinal/4.jpg', '/CineKinal2013173/ServletDorian.do?idPelicula=8');
 
 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'The amazing spiderman', 'Un joven llamado Peter Parker es abandonado por sus padres, que lo dejan con sus tios, Ben y May, y luego se marchan sin dar ninguna explicacion. anos despues, un ano adolescente Peter asiste al Instituto de Ciencias Miltdown, donde es acosado por el maton Flash Thompson y se siente atraido por Gwen Stacy. Un dia, Peter encuentra unos papeles de su padre, Richard Parker, y descubre que estuvo trabajando en Industrias Oscorp con el Dr. Curtis Connors, un cientifico con un brazo incompleto.', 'El Sorprendente Hombre Arana es la historia de Peter Parker, un estudiante adolescente marginado que fue abandonado por sus padres cuando era nino y criado por su t�o Ben y su t�a May. Como muchos adolescentes, Peter trata de descubrir qui�n es y c�mo lleg� a ser la persona que es hoy. Peter tambi�n est� buscando su camino con su primer amor Gwen Stacy, y juntos, ellos luchan, con amor, compromiso y secretos. En cuanto Peter encuentra un misterioso portafolio que perteneci� a su padre, �l comienza una b�squeda para entender la desaparici�n de sus padres - que lo lleva directamente a Oscorp y el laboratorio de Dr. Curt Connors, ex-socio de su padre. Anhelando respuestas y una conexi�n, Peter comete un error que lo pondr� en curso de colisi�n con el alter-ego de Connor, The Lizard. Como El Hombre Arana, Peter har� elecciones dr�sticas para usar sus poderes y moldear su destino para convertirse en un h�roe.', 1.59,'/CineKinal2013173/CineKinal/5.jpg', '/CineKinal2013173/ServletSpider.do?idPelicula=10');

 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Superman', 'En el remoto y condenado planeta Kripton, durante un juicio al dictador General Zod (Terence Stamp) y sus secuaces Ursa (Sarah Douglas) y Non (Jack OHalloran), estos son condenados a la Zona Fantasma gracias a las pruebas de sedicion y rebelion demostradas por Jor-El (Marlon Brando), cientifico y alto cargo del Consejo, y por la decision del consejo de Krypton.', 'En el remoto y condenado planeta Kripton, durante un juicio al dictador General Zod (Terence Stamp) y sus secuaces Ursa (Sarah Douglas) y Non (Jack OHalloran), estos son condenados a la Zona Fantasma gracias a las pruebas de sedici�n y rebeli�n demostradas por Jor-El (Marlon Brando), cient�fico y alto cargo del Consejo, y por la decisi�n del consejo de Krypton. Antes Zod pide a Jor-El unirsele ya que tambi�n discrepa con en consejo pero al rechazarle jura venganza contra Jor-El y sus descendientes mientras desaparece transportado a otra dimensi�n. Zod amenaza con volver
	 					El alto consejero (Trevor Howard) y el segundo (Harry Andrews) felicitan a Jor-El haber encerrado a los tres criminales en la Zona Fantasma. Despu�s Jor-El intenta convencer al m�ximo �rgano de Krypton que se debe hacer una evacuaci�n inmediata debido a que la inestabilidad del uranio que compone el n�cleo del planeta causar� la destrucci�n de este tarde o temprano.', 2.41, '/CineKinal2013173/CineKinal/6.jpg' , '/CineKinal2013173/ServletSuperman.do?idPelicula=12');

 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Iron Man', 'Tony Stark, un mujeriego y genio, quien heredo el contrato de propiedad de Industrias Stark por parte de su padre Howard Stark, se encuentra en la guerra de Afganistán con su amigo militar el teniente coronel James Rhodes para demostrar el funcionamiento de su nuevo misil Jerico. Stark es seriamente lastimado en una emboscada (ironicamente por una de sus armas) y es secuestrado y llevado a una cueva por el grupo terrorista de los Diez Anillos.', 'Tony Stark, un mujeriego y genio, quien hered� el contrato de propiedad de Industrias Stark por parte de su padre Howard Stark, se encuentra en la guerra de Afganist�n con su amigo militar el teniente coronel James Rhodes para demostrar el funcionamiento de su nuevo misil �Jeric�. Stark es seriamente lastimado en una emboscada (ir�nicamente por una de sus armas) y es secuestrado 
	 					 y llevado a una cueva por el grupo terrorista de los Diez Anillos.', 2.14, '/CineKinal2013173/CineKinal/7.jpg', '/CineKinal2013173/ServletIron.do?idPelicula=14');

 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Capitan America', 'En la actualidad, unos cientificos en el Artico descubren un objeto circular congelado con un adorno rojo, blanco y azul parecido a un escudo, los cientificos descubren que ahi se encontraba congelado Steve Rogers, mejor conocido como el Capitan America.', 'En la actualidad, unos cient�ficos en el �rtico descubren un objeto circular congelado con un adorno rojo, blanco y azul parecido a un escudo, los cient�ficos descubren que ah� se encontraba congelado Steve Rogers, mejor conocido como el Capit�n Am�rica.
						 En marzo de 1942, el oficial nazi Johann Schmidt y sus hombres entran en la ciudad de Sandefjord en Noruega ocupada por los alemanes, para robar un misterioso dispositivo llamado Tesseracto, que posee poderes incalculables. Mientras tanto, en la ciudad de Nueva York, Steve Rogers es rechazado para el reclutamiento militar de la Segunda Guerra Mundial debido a diversos problemas f�sicos y de salud. 
					     Mientras asist�a a una exposici�n de tecnolog�as del futuro con su amigo, el sargento James "Bucky" Barnes, Rogers vuelve a intentar alistarse. Al o�r la conversaci�n de Rogers con Barnes sobre el deseo de ayudar en la guerra, el Dr. Abraham Erskine permite a Rogers alistarse.', 3.21 , '/CineKinal2013173/CineKinal/8.jpg', '/CineKinal2013173/ServeltCapitan.do?idPelicula=16');

 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Hulk', 'David Banner es un investigador de la genetica que ha descubierto la manera de mutar el ADN humano para que el cuerpo pueda curarse rapidamente de una lesion o herida. Desea usar su investigacion para crear supersoldados para el Ejercito de los Estados Unidos, pero no le es permitido realizarlo, asi que experimenta en si mismo.', 'David Banner es un investigador de la gen�tica que ha descubierto la manera de mutar el ADN humano para que el cuerpo pueda curarse r�pidamente de una lesi�n o herida. Desea usar su investigaci�n para crear supersoldados para el Ej�rcito de los Estados Unidos, pero no le es permitido realizarlo, as� que experimenta en s� mismo. Una vez que su esposa da a luz a su hijo Bruce, David se da cuenta que su ADN mutante se ha transmitido e intenta encontrar una cura para la condici�n de su hijo. En 1973, el gobierno, 
						 representado por el entonces General Thaddeus E. "Trueno" Ross, pone a fin su investigaci�n despu�s de enterarse de sus peligrosos experimentos. David, en un arranque de ira, causa una explosi�n masiva de las instalaciones del reactor de rayos gamma, e intenta asesinar a su hijo, accidentalmente matando a su esposa cuando ella interfiere. Entonces, David es llevado a un hospital mental, mientras el Bruce de 4 a�os es enviado en acogida y adoptado, reprimiendo el recuerdo de sus padres biol�gicos, creyendo que ambos han muerto. 
						', 2.14, '/CineKinal2013173/CineKinal/9.jpg', '/CineKinal2013173/ServletHulk.do?idPelicula=18');


 INSERT INTO Pelicula(IDPELICULA, NOMBREPELICULA, descripcionPelicula, trailerPelicula, duracionPelicula, RUTAIMG, direccionServlet)
  VALUES (Pelicula_seq.NEXTVAL, 'Thor', 'En 965 d.C., Odin (Anthony Hopkins), rey de Asgard, libra una guerra contra los Gigantes de Hielo de Jotunheim y contra su lider Laufey (Colm Feore), para evitar que conquisten los Nueve Reinos, comenzando con la Tierra. Los guerreros asgardianos derrotan a los Gigantes de Hielo y aprovechan la fuente de su poder, el Cofre de los Viejos Inviernos.', 'En 965 d.C., Odin (Anthony Hopkins), rey de Asgard, libra una guerra contra los Gigantes de Hielo de Jotunheim y contra su l�der Laufey (Colm Feore), para evitar que conquisten los Nueve Reinos, comenzando con la Tierra. Los guerreros asgardianos derrotan a los Gigantes de Hielo y aprovechan la fuente de su poder, el Cofre de los Viejos Inviernos.
				     En el presente, el hijo de Odin, Thor (Chris Hemsworth) se prepara para ascender al trono de Asgard, pero es interrumpido cuando los Gigantes de Hielo intentan recuperar el cofre pero son detenidos por Destructor, un antiguo guardian de la Camara de Antiguedades. 
				     Desobedeciendo la orden de Odin, Thor viaja a Jotunheim para hacer frente a Laufey, acompa�ado por su hermano Loki (Tom Hiddleston), su amiga de la infancia Sif (Jaimie Alexander) y los Tres Guerreros: Volstagg (Ray Stevenson), Fandral (Joshua Dallas) y Hogun (Tadanobu Asano).', 3.21 , '/CineKinal2013173/CineKinal/10.jpg', '/CineKinal2013173/ServeltThor.do?idPelicula=20');
					      
--Categoria

INSERT INTO Categoria(IDCATEGORIA, nombreCategoria, descriptionCategoria)
  VALUES (categ_seq.NEXTVAL, 'PG17', 'Personas mayores de 17 anios');
  
 
INSERT INTO Categoria(IDCATEGORIA, nombreCategoria, descriptionCategoria)
  VALUES (categ_seq.NEXTVAL, 'PG14', 'Personas mayores de 14 anios');
 
 
INSERT INTO Categoria(IDCATEGORIA, nombreCategoria, descriptionCategoria)
  VALUES (categ_seq.NEXTVAL, 'PG18', 'Solo personas adultas debido a su contenido');

 --Clasificacion 

INSERT INTO Clasificacion(IDCLASIFICACION, NOMBRECLASIFICACION, descripcionClasificacion)
  VALUES (clasif_seq.NEXTVAL, 'Terror', 'Sumergete en el drama y el suspenso');
  
INSERT INTO Clasificacion(IDCLASIFICACION, NOMBRECLASIFICACION, descripcionClasificacion)
  VALUES (clasif_seq.NEXTVAL, 'Aventura', 'Vive una aventura inimaginable');
  
INSERT INTO Clasificacion(IDCLASIFICACION, NOMBRECLASIFICACION, descripcionClasificacion)
  VALUES (clasif_seq.NEXTVAL, 'Fantasia', 'Peliculas de excelencia');

--Funcion
  
  INSERT INTO Funcion(IDFUNCION, NOMBREFUNCION, descripcionFuncion, presioFuncion)
    VALUES (funcion_seq.NEXTVAL, 'Primera funcion', 'Primera funcion del cine', 50);
    
  INSERT INTO Funcion(IDFUNCION, NOMBREFUNCION, descripcionFuncion, presioFuncion)
    VALUES (funcion_seq.NEXTVAL, 'Segunda funcion', 'Segunda funcion del cine', 60);
    
    
  INSERT INTO Funcion(IDFUNCION, NOMBREFUNCION, descripcionFuncion, presioFuncion)
    VALUES (funcion_seq.NEXTVAL, 'Tercera funcion', 'Tercera funcion del cine', 70);
    
    
  INSERT INTO Funcion(IDFUNCION, NOMBREFUNCION, descripcionFuncion, presioFuncion)
    VALUES (funcion_seq.NEXTVAL, 'Cuarta funcion', 'Cuarta funcion del cine', 70);
    
--Sala
  
	INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 2, 'Sala 4', 'VIP');
    
	INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 3', '3d');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 2, 'Sala 1', 'Normal');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 2, 'Sala 2', 'Normal');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 2, 'Sala 4', '3d');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 2, 'Sala 5', '3d');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 1', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 2', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 3', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 4', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 5', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 6', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 1', '3d');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 6, 'Sala 1', '3d');
 
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 6, 'Sala 1', 'vip');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 2, 'Sala 4', 'VIP');
    
	INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 4, 'Sala 3', '3d');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 8, 'Sala 1', 'Normal');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 8, 'Sala 2', 'Normal');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 8, 'Sala 4', '3d');
    
    INSERT INTO Sala(IDSALA,IDCOMPLEJO,nombreSala, tipoSala)
    VALUES (Sala_seq.NEXTVAL, 8, 'Sala 5', '3d');
    
    --Cartelera
	
	INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 2, 2, '12:30', '02:10');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 4, 5, '14:30', '15:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 2, 8, '17:10', '18:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 22, 10, '15:10', '16:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 24, 12, '17:10', '19:50');
        
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 56, 4, '14:30', '15:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 54, 6, '17:10', '18:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 50, 14, '15:10', '16:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 48, 16, '17:10', '19:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 44, 18, '19:20', '20:50');
    
    INSERT INTO Cartelera(IDCARTELERA,IDSALA,idPelicula, horaInicio, horaFin)
    VALUES (cartelera_seq.NEXTVAL, 22, 20, '19:20', '20:50');
    