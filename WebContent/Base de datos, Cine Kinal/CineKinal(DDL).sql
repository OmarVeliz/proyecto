CREATE TABLE Categoria(
  idCategoria INT not null,
  nombreCategoria VARCHAR(10) not null,
  descriptionCategoria VARCHAR(255) not null,
  PRIMARY KEY(idCategoria)
) 

CREATE SEQUENCE categ_seq;

CREATE OR REPLACE TRIGGER Categoria_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW

BEGIN
  SELECT categ_seq.NEXTVAL
  INTO   :new.idCategoria
  FROM   dual;
END;

CREATE TABLE Clasificacion(
  idClasificacion INT not null,
  nombreClasificacion VARCHAR(20),
  descripcionClasificacion VARCHAR(255),
  PRIMARY KEY(idClasificacion)
)


CREATE SEQUENCE clasif_seq;

CREATE OR REPLACE TRIGGER Clasificacion_bir 
BEFORE INSERT ON Clasificacion
FOR EACH ROW

BEGIN
  SELECT clasif_seq.NEXTVAL
  INTO   :new.idClasificacion
  FROM   dual;
END;

CREATE TABLE Funcion(
  idFuncion INT not null,
  nombreFuncion VARCHAR(25) not null,
  descripcionFuncion VARCHAR(255) not null,
  presioFuncion VARCHAR(255) not null,
  PRIMARY KEY(idFuncion)
)

CREATE SEQUENCE funcion_seq;

CREATE OR REPLACE TRIGGER Funcion_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT funcion_seq.NEXTVAL
  INTO   :new.idFuncion
  FROM   dual;
END;

CREATE TABLE Venta(
  idVenta INT not null,
  nombrePersona VARCHAR(25) not null,
  numeroTransaccion INT not null,
  correoPersona VARCHAR(25) not null,
  telefonoPersona int not null,
  tipoPago VARCHAR(25) not null,
  PRIMARY KEY(idVenta)
)

CREATE SEQUENCE venta_seq;

CREATE OR REPLACE TRIGGER Venta_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT venta_seq.NEXTVAL
  INTO   :new.idVenta
  FROM   dual;
END;

CREATE TABLE Complejo(
  idComplejo NUMBER not null,
  nombreComplejo VARCHAR(25) not null,
  direccionComplejo VARCHAR(25) not null,
  callCenter INT not null,
  latitudComplejo VARCHAR(25),
  PRIMARY KEY(idComplejo)
)

CREATE SEQUENCE Complejo_seq;

CREATE OR REPLACE TRIGGER complejo_bir 
BEFORE INSERT ON Complejo
FOR EACH ROW

BEGIN
  SELECT Complejo_seq.NEXTVAL
  INTO   :new.idComplejo
  FROM   dual;
END;

CREATE TABLE Sala(
  idSala INT not null,
  idComplejo INT not null,
  nombreSala VARCHAR(25) not null,
  tipoSala VARCHAR(10) not null,
  PRIMARY KEY(idSala),
  CONSTRAINT Sala_fk FOREIGN KEY(idComplejo) REFERENCES Complejo(idComplejo)
)

CREATE SEQUENCE Sala_seq;

CREATE OR REPLACE TRIGGER sala_bir 
BEFORE INSERT ON Sala
FOR EACH ROW

BEGIN
  SELECT Sala_seq.NEXTVAL
  INTO   :new.idSala
  FROM   dual;
END;


CREATE TABLE Asiento(
  idAsiento INT not null,
  idSala INT not null,
  nombreAsiento VARCHAR(10) not null,
  PRIMARY KEY(idAsiento),
  CONSTRAINT Asiento_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala)
)

CREATE SEQUENCE Asiento_seq;

CREATE OR REPLACE TRIGGER Asiento_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT Asiento_seq.NEXTVAL
  INTO   :new.idAsiento
  FROM   dual;
END;

CREATE TABLE Pelicula(
  idPelicula INT not null,
  nombrePelicula VARCHAR(50) not null,
  descripcionPelicula VARCHAR(255) not null,
  trailerPelicula VARCHAR(255) not null,
  duracionPelicula NUMBER(6,2) not null,
  rutaImg Varchar(25),
  PRIMARY KEY(idPelicula)
)

CREATE SEQUENCE Pelicula_seq;

CREATE OR REPLACE TRIGGER Pelicula_bir 
BEFORE INSERT ON Pelicula
FOR EACH ROW

BEGIN
  SELECT Pelicula_seq.NEXTVAL
  INTO   :new.idPelicula
  FROM   dual;
END;

CREATE TABLE Cartelera(
  idCartelera INT not null,
  idSala INT not null,
  idPelicula INT not null,
  horaInicio NUMBER(6,2) not null,
  horaFin NUMBER(6,2) not null,
  PRIMARY KEY(idCartelera),
  CONSTRAINT PeliculaSala_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala),
  CONSTRAINT CarteleraPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE cartelera_seq;

CREATE OR REPLACE TRIGGER Cartelera_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT cartelera_seq.NEXTVAL
  INTO   :new.idCartelera
  FROM   dual;
END;

CREATE TABLE PeliculaCategoria(
  idPelicula INT not null,
  idCategoria INT NOT NULL,
  CONSTRAINT CategoriaPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula),
  CONSTRAINT PeliculaCateria_fk FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
)

CREATE TABLE VentaDetalle(
  idVenta INT not null,
  idAsiento INT not null,
  idCartelera INT not null,
  idFuncion INT not null,
  CONSTRAINT VentaDetalleVenta_fk FOREIGN KEY(idVenta) REFERENCES Venta(idVenta),
  CONSTRAINT VentaDetalleAsiento_fk FOREIGN KEY(idAsiento) REFERENCES Asiento(idAsiento),
  CONSTRAINT VentaCartelera_fk FOREIGN KEY(idCartelera) REFERENCES Cartelera(idCartelera),
  CONSTRAINT VentaFuncion_fk FOREIGN KEY(idFuncion) REFERENCES Funcion(idFuncion)
)

CREATE TABLE EstrenoPelicula(
  idEstreno INT not null,
  idPelicula INT not null,
  fechaPelicula DATE not null,
  PRIMARY KEY(idEstreno),
  CONSTRAINT EstrenoPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE Estreno_seq;

CREATE OR REPLACE TRIGGER Estreno_bir 
BEFORE INSERT ON EstrenoPelicula
FOR EACH ROW

BEGIN
  SELECT Estreno_seq.NEXTVAL
  INTO   :new.idEstreno
  FROM   dual;
END;

SELECT * FROM Pelicula;

_______________________________________________________________________________

CREATE TABLE Categoria(
  idCategoria NUMBER not null,
  nombreCategoria VARCHAR(10) not null,
  descriptionCategoria VARCHAR(255) not null,
  PRIMARY KEY(idCategoria)
) 

CREATE SEQUENCE categ_seq;

CREATE OR REPLACE TRIGGER Categoria_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW

BEGIN
  SELECT categ_seq.NEXTVAL
  INTO   :new.idCategoria
  FROM   dual;
END;

CREATE TABLE Clasificacion(
  idClasificacion NUMBER not null,
  nombreClasificacion VARCHAR(20),
  descripcionClasificacion VARCHAR(255),
  PRIMARY KEY(idClasificacion)
)


CREATE SEQUENCE clasif_seq;

CREATE OR REPLACE TRIGGER Clasificacion_bir 
BEFORE INSERT ON Clasificacion
FOR EACH ROW

BEGIN
  SELECT clasif_seq.NEXTVAL
  INTO   :new.idClasificacion
  FROM   dual;
END;

CREATE TABLE Funcion(
  idFuncion NUMBER not null,
  nombreFuncion VARCHAR(25) not null,
  descripcionFuncion VARCHAR(255) not null,
  presioFuncion VARCHAR(255) not null,
  PRIMARY KEY(idFuncion)
)

CREATE SEQUENCE funcion_seq;

CREATE OR REPLACE TRIGGER Funcion_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT funcion_seq.NEXTVAL
  INTO   :new.idFuncion
  FROM   dual;
END;

CREATE TABLE Venta(
  idVenta NUMBER not null,
  nombrePersona VARCHAR(25) not null,
  numeroTransaccion NUMBER not null,
  correoPersona VARCHAR(25) not null,
  telefonoPersona NUMBER not null,
  tipoPago VARCHAR(25) not null,
  PRIMARY KEY(idVenta)
)


CREATE SEQUENCE venta_seq;

CREATE OR REPLACE TRIGGER Venta_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT venta_seq.NEXTVAL
  INTO   :new.idVenta
  FROM   dual;
END;

 CREATE TABLE Complejo(
  idComplejo NUMBER not null,
  nombreComplejo VARCHAR(25) not null,
  direccionComplejo VARCHAR(25) not null,
  callCenter INT not null,
  latitudComplejo VARCHAR(100),
  longitudComplejo VARCHAR(100),
  rutaServerlet VARCHAR(100),
  PRIMARY KEY(idComplejo)
)

CREATE SEQUENCE Complejo_seq;

CREATE OR REPLACE TRIGGER complejo_bir 
BEFORE INSERT ON Complejo
FOR EACH ROW

BEGIN
  SELECT Complejo_seq.NEXTVAL
  INTO   :new.idComplejo
  FROM   dual;
END;

CREATE TABLE Sala(
  idSala NUMBER not null,
  idComplejo NUMBER not null,
  nombreSala VARCHAR(25) not null,
  tipoSala VARCHAR(10) not null,
  PRIMARY KEY(idSala),
  CONSTRAINT Sala_fk FOREIGN KEY(idComplejo) REFERENCES Complejo(idComplejo)
)

CREATE SEQUENCE Sala_seq;

CREATE OR REPLACE TRIGGER sala_bir 
BEFORE INSERT ON Sala
FOR EACH ROW

BEGIN
  SELECT Sala_seq.NEXTVAL
  INTO   :new.idSala
  FROM   dual;
END;


CREATE TABLE Asiento(
  idAsiento NUMBER not null,
  idSala NUMBER not null,
  nombreAsiento VARCHAR(10) not null,
  PRIMARY KEY(idAsiento),
  CONSTRAINT Asiento_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala)
)

CREATE SEQUENCE Asiento_seq;

CREATE OR REPLACE TRIGGER Asiento_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT Asiento_seq.NEXTVAL
  INTO   :new.idAsiento
  FROM   dual;
END;

  CREATE TABLE Pelicula(
  idPelicula INT not null,
  nombrePelicula VARCHAR(50) not null,
  descripcionPelicula VARCHAR(1700) not null,
  trailerPelicula VARCHAR(1700) not null,
  duracionPelicula NUMBER(6,2) not null,
  rutaImg Varchar(100),
  direccionServlet VARCHAR(100),
  PRIMARY KEY(idPelicula)
  )	


  CREATE SEQUENCE Pelicula_seq;

  CREATE OR REPLACE TRIGGER Pelicula_bir 
  BEFORE INSERT ON Pelicula
  FOR EACH ROW

  BEGIN
  SELECT Pelicula_seq.NEXTVAL
  INTO   :new.idPelicula
  FROM   dual;
  END;

CREATE TABLE Cartelera(
  idCartelera NUMBER not null,
  idSala NUMBER not null,
  idPelicula NUMBER not null,
  horaInicio VARCHAR(25) not null,
  horaFin VARCHAR(25) not null,
  PRIMARY KEY(idCartelera),
  CONSTRAINT PeliculaSala_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala),
  CONSTRAINT CarteleraPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE cartelera_seq;

CREATE OR REPLACE TRIGGER Cartelera_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT cartelera_seq.NEXTVAL
  INTO   :new.idCartelera
  FROM   dual;
END;

CREATE TABLE PeliculaCategoria(
  idPelicula NUMBER not null,
  idCategoria NUMBER NOT NULL,
  CONSTRAINT CategoriaPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula),
  CONSTRAINT PeliculaCateria_fk FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
)

CREATE TABLE VentaDetalle(
  idVenta NUMBER not null,
  idAsiento NUMBER not null,
  idCartelera NUMBER not null,
  idFuncion NUMBER not null,
  CONSTRAINT VentaDetalleVenta_fk FOREIGN KEY(idVenta) REFERENCES Venta(idVenta),
  CONSTRAINT VentaDetalleAsiento_fk FOREIGN KEY(idAsiento) REFERENCES Asiento(idAsiento),
  CONSTRAINT VentaCartelera_fk FOREIGN KEY(idCartelera) REFERENCES Cartelera(idCartelera),
  CONSTRAINT VentaFuncion_fk FOREIGN KEY(idFuncion) REFERENCES Funcion(idFuncion)
)

CREATE TABLE EstrenoPelicula(
  idEstreno NUMBER not null,
  idPelicula NUMBER not null,
  fechaPelicula DATE not null,
  PRIMARY KEY(idEstreno),
  CONSTRAINT EstrenoPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE Estreno_seq;

CREATE OR REPLACE TRIGGER Estreno_bir 
BEFORE INSERT ON EstrenoPelicula
FOR EACH ROW

BEGIN
  SELECT Estreno_seq.NEXTVAL
  INTO   :new.idEstreno
  FROM   dual;
END;

SELECT * FROM Pelicula;
